from books_app.views import AuthorQueryView, BookAuthorSearch, BookNameSearch, BookQueryView, Home
from django.urls import path

urlpatterns = [
        path('',Home.as_view(), name='home'),
        path('book-name-search',BookNameSearch.as_view(), name='book_name_search'),
        path('book-author-search',BookAuthorSearch.as_view(), name='book_author_search'),
        
        path('book-name-query',BookQueryView.as_view(), name='book_name_query'),
        path('author-name-query',AuthorQueryView.as_view(), name='author_name_query'),
        ]
