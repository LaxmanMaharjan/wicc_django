from django.contrib import admin
from .models import Author, Book, Genre

# Register your models here.
class AuthorAdmin(admin.ModelAdmin):
    list_display = ['name','get_books']


class BookAdmin(admin.ModelAdmin):
    list_display = ['name','price','count']

class GenreAdmin(admin.ModelAdmin):
    list_display = ['name','book']

admin.site.register(Author,AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Genre, GenreAdmin)
