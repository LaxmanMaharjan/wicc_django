from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from .models import Author, Book, Genre
from .forms import BookForm, AuthorForm
from django.http.response import JsonResponse
from django.core import serializers

# Create your views here.
class Home(TemplateView):
    template_name = 'books_app/index.html'

class BookNameSearch(TemplateView):
    template_name = 'books_app/book.html'

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        context['form'] = BookForm()
        return context

class BookAuthorSearch(TemplateView):
    template_name = 'books_app/author.html'

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        context['form'] = AuthorForm()
        return context

class AuthorQueryView(View):

    def post(self,request):
        author_name = request.POST.get('author_name')
        author = Author.objects.get(name = author_name)
        print(author.get_books_list())
        print(type(author))
        form = AuthorForm(request.POST)

        #if len(books) == 0:
        #    books = [{'name':'No Books Found','price':0.00,'count':0},]
        #    ser_instance = serializers.serialize('json', books) # list of dictionary with necessary info
        #    return JsonResponse({'instance': ser_instance,})

        if form.is_valid():
            # Model.save() does either INSERT or UPDATE of an object in a DB, while Model.objects.create() does only INSERT.
            instance = {'name':author.name,'book':author.get_books_list()}
            print(instance)
            return JsonResponse({'instance': instance,}, safe=False)

class BookQueryView(View):

    def post(self,request):
        book_name = request.POST.get('book_name')
        books = Book.objects.filter(name = book_name)
        print(len(books) == 0)
        form = BookForm(request.POST)

        #if len(books) == 0:
        #    books = [{'name':'No Books Found','price':0.00,'count':0},]
        #    ser_instance = serializers.serialize('json', books) # list of dictionary with necessary info
        #    return JsonResponse({'instance': ser_instance,})

        if form.is_valid():
            # Model.save() does either INSERT or UPDATE of an object in a DB, while Model.objects.create() does only INSERT.
            ser_instance = serializers.serialize('json', books) # list of dictionary with necessary info
            return JsonResponse({'instance': ser_instance,})
        
