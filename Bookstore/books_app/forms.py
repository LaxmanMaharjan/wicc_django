from django import forms

class BookForm(forms.Form):
    book_name = forms.CharField(max_length=100)

class AuthorForm(forms.Form):
    author_name = forms.CharField(max_length= 100)
