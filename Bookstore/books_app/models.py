from django.db import models

# Create your models here.
class Book(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField()
    count = models.IntegerField()

    def __str__(self):
        return self.name

class Genre(models.Model):
    name = models.CharField(max_length = 100)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class Author(models.Model):
    name = models.CharField(max_length = 100)
    book = models.ManyToManyField(Book)

    def __str__(self):
        return f"{self.name} => {self.book.name}"

    def get_books(self):
        return "\n".join([b.name for b in self.book.all()])

    def get_books_list(self):
        return [b.name for b in self.book.all()]

