from django.contrib import admin
from .models import FeedbackModel, ArticleModel

# Register your models here
class FeedbackModelAdmin(admin.ModelAdmin):
   pass 

class ArticleModelAdmin(admin.ModelAdmin):
   pass 

admin.site.register(FeedbackModel, FeedbackModelAdmin)
admin.site.register(ArticleModel, ArticleModelAdmin)
