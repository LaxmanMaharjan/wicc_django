from django import forms
from django.core.exceptions import ValidationError
from django.db.models import fields
from .titles import title_list
from .models import ArticleModel, FeedbackModel

class AboutUsForm(forms.Form):
    title = forms.CharField(max_length=100, required=True, label='Title')
    description = forms.CharField()

#class ArticleForm(forms.Form): 
#    title = forms.CharField(max_length=100, required=True, label='Title')
#    content = forms.CharField(widget=forms.Textarea, max_length=200, required=True, label='Content')
#
#    def __init__(self, *args, **kwargs):
#        super(ArticleForm, self).__init__(*args, **kwargs)
#        self.title_list = title_list
#        for field in self.fields:
#            self.fields[field].widget.attrs.update({'class':'form-control'})
#
#    def clean(self):
#        cleaned_data = super().clean()
#        title = self.cleaned_data['title']
#        #print('Value of Title form clean method in forms',title)
#        #print(type(title))
#        for title1 in self.title_list:
#            if title1 == title:
#                raise ValidationError('Title Already Exists.')
#
#        return cleaned_data
class ArticleForm(forms.ModelForm): 
    
    class Meta:
        model = ArticleModel
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)
        self.title_list = title_list
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class':'form-control'})

    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data['title']
        #print('Value of Title form clean method in forms',title)
        #print(type(title))
        title_list = ArticleModel.objects.values_list('title', flat=True)
        title = self.cleaned_data['title']
        if title in title_list:
                raise ValidationError('Title Already Exists.')

        return cleaned_data

class FeedbackForm(forms.Form):
    title = forms.CharField(max_length=100, required=True, label='Title')
    content = forms.CharField(widget=forms.Textarea, max_length=200, required=True, label='Content')

    def __init__(self, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)
        self.title_list = title_list
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class':'form-control form-control-sm'})

    def clean(self):
        cleaned_data = super().clean()
        title_list = FeedbackModel.objects.values_list('title', flat=True)
        title = self.cleaned_data['title']
        if title in title_list:
                raise ValidationError('Title Already Exists.')

        return cleaned_data

class RegistrationForm(forms.Form):
    email = forms.EmailField()
