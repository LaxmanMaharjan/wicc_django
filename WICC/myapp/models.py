from django.db import models

# Create your models here.
class FeedbackModel(models.Model):
    title = models.CharField(max_length=100, default='', null=False, blank=False)
    content = models.CharField(max_length=200, default='', null=False, blank=False)

    def __str__(self):
        return self.title

class ArticleModel(models.Model):
    title = models.CharField(max_length=100, default='', null=False, blank=False)
    content = models.CharField(max_length=500, default='', null=False, blank=False)

    def __str__(self):
        return self.title
