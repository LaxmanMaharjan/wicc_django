from django.shortcuts import redirect, render
from django.urls.base import reverse_lazy
from django.views import View
from .forms import AboutUsForm, ArticleForm, FeedbackForm, RegistrationForm
from .titles import title_list
from django.views.generic import FormView
from .models import FeedbackModel, ArticleModel
from django.core.mail import send_mail

# Create your views here.

def home_view(request):
    context = {'name':'Laxman Maharjan', 'age':23, 'address':'Tahachal',
            'about_job':{
                'company':'Info_Dev',
                'address':'Sanepa'
                },
            'names':['ana','luke','derek']
            
            }

    return render(request,'index.html',context)

def about_view(request):
    form = AboutUsForm()
    context1 = {'name':'Laxman Maharjan', 'age':23, 'address':'Tahachal',
            'about_job':{
                'company':'Info_Dev',
                'address':'Sanepa'
                },
            'names':['ana','luke','derek']
            }
    context = {'form':form}
    if request.method == "GET":
        print(request.GET)
        print('This is get request.')
    else:
        print('This is post request')
        return redirect("/")

    print(request.method,"#######")
    return render(request,'about.html', context)

def contact_view(request):
    if request.method == "POST":
        email = request.POST.get('email')
        message = request.POST.get('message')
        context ={'email':email, 'message':message}        
        return render(request, 'contact.html', context)
    
    return render(request, 'contact.html')

class ArticleView(FormView):
    form_class = ArticleForm
    template_name = 'article.html'
    success_url = reverse_lazy('article')

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        context['message'] = 'This is Article Page.'
        context['articles'] =ArticleModel.objects.all()
        return context

    def form_invalid(self, form):
        print('This form is invalid')
        return super().form_invalid(form)

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

class FeedbackView(FormView):
    form_class = FeedbackForm
    template_name = 'feedback.html'
    success_url = reverse_lazy('feedback')

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        context['message'] = 'This is test message.'
        context['feedbacks'] =FeedbackModel.objects.all()
        return context

    def form_invalid(self, form):
        print('This form is invalid')
        return super().form_invalid(form)

    def form_valid(self, form):
        title = form.cleaned_data['title']
        content = form.cleaned_data['content']
        print(title,content)
        f = FeedbackModel(title=title, content=content)
        f.save()
        return super().form_valid(form)


#class ArticleView(View):
#    article_form=ArticleForm()
#    context={ 'article_form':article_form,
#                 'article_list':[
#        {
#            "title": "First",
#            "content": "This is the first article",
#            
#        },
#        {
#            "title": "Second",
#            "content": "This is the Second article",
#        },
#        {
#            "title": "Third",
#            "content": "This is the Third article",
#           
#        },
#        {
#            "title": "Forth",
#            "content": "This is the Fourth article",
#           
#        }]
#        }
#    def get(self, request):
#            return render(request, 'article.html', self.context)
#
#    def post(self, request):
#        #print(request.POST)
#        form = ArticleForm(request.POST)
#        if form.is_valid():
#            #print("Form is Valid")
#            title = form.cleaned_data['title']
#            content = form.cleaned_data['content']
#            new_article={
#                    'title':title,
#                    'content': content,
#                    }
#            self.context['article_list'].append(new_article)
#            title_list.append(title)
#            #print('post method')
#            #print(new_article)
#            self.context['article_form']=form
#
#        else:
#            self.context['article_form']=form
#            #print(form.errors)
#            #print('Form is not Valid')
#        return render(request, 'article.html', self.context)

#def article_view(request):
#    if request.method == "POST":
#        print(request.POST)
#        form = ArticleForm(request.POST)
#        if form.is_valid():
#            title = form.cleaned_data['title']
#            content = form.cleaned_data['content']
#            new_article={
#                    'title':title,
#                    'content': content,
#                    
#                    }
#            context['article_list'].append(new_article)
#            print('post method')
#            print(title)
#        return render(request, 'article.html', context)
#
#    else:
#        return render(request, 'article.html', context)
def student_detail_view(request):
    context = {'students':[
        
        {'id':1,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':2,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':3,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':4,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':5,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':6,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':7,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':8,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':9,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        {'id':10,
         'name':'Laxman Maharjan',
         'roll_no':20,
         'address':'Tahachal',
         'number':9849559082,
         'email':'lxmnmrzn@gmail.com'
            },
        ]

            }
    return render(request,'student_details.html', context)


context = {"expenses":[
                {
                'title':'Movie Watching',
                'amount':500
                },
                {
                    'title':'Travelling',
                    'amount':100
                    },
                {
                    'title':'Eating',
                    'amount':100
                    }
                ]
            }
class ExpenseListView(View):
    def get(self, request): 
        print(context)
        return render(request, 'expense_list.html', context)    

    def post(self, request):
        title = request.POST.get('title')
        amount = request.POST.get('amount')
        context['expenses'].append({'title':title, 'amount':amount})

        return redirect('/expense_list/')

class ExpenseDetailView(View):
    def get(self, request, title):
        context1 = None
        for expense in context['expenses']:
            if expense['title'] == title:
                context1 = expense
                break
        return render(request, 'expense_detail.html', context1)
    
    
class RegistrationFormView(FormView):
    form_class = RegistrationForm 
    template_name = 'registration.html'
    success_url = reverse_lazy('registration')

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        context['message'] = 'Email is sent Successfully.'
        return context
    
    def form_invalid(self, form):
        print('form_invalid')
        return super().form_invalid(form)

    def form_valid(self, form):
        email = form.cleaned_data['email']

        send_mail(
            'Email from Django', #Subject
            'Learning how to send Email from Django.', #Message
            'lxmnmrzn17@gmail.com',  #From
            [email],  #To
            fail_silently=False,
        )
        print('email')
        print(email)
        return super().form_valid(form)
