"""WICC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from myapp.views import FeedbackView, ArticleView, ExpenseDetailView, ExpenseListView, RegistrationFormView, home_view,about_view,contact_view, student_detail_view

from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',home_view, name='home'),
    path('about/',about_view, name='about'),
    path('contact/',contact_view, name='contact'),
    path('article/', ArticleView.as_view(), name='article'),
    path('feedback/', FeedbackView.as_view(), name='feedback'),
    path('student-detail/',student_detail_view, name='student_detail'),
    path('expense_list/', ExpenseListView.as_view(), name='expense_list'),
    path('expense_detail/<str:title>', ExpenseDetailView.as_view(), name='expense_detail'),
    path('registration', RegistrationFormView.as_view(), name='registration'),
]
