from django.contrib import admin
from .models import User,Book,LibraryBooks,Genre
# Register your models here.

class UserAdmin(admin.ModelAdmin):
    pass

class LibraryBooksAdmin(admin.ModelAdmin):
    list_display = ['title', 'popularity','borrowed']

class BookAdmin(admin.ModelAdmin):
    list_display = ['user','date','library_book']

class GenreAdmin(admin.ModelAdmin):
    list_display = ['name','books']

admin.site.register(User, UserAdmin)
admin.site.register(Book,BookAdmin)
admin.site.register(LibraryBooks,  LibraryBooksAdmin)
admin.site.register(Genre, GenreAdmin)
