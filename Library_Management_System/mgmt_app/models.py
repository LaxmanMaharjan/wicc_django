from django.db import models
from django.utils.regex_helper import Choice

# Create your models here.
class User(models.Model):
    roll_no = models.CharField(max_length = 20, primary_key = True)
    username = models.CharField(max_length=100)
    email = models.EmailField()
    password = models.CharField(max_length = 100)
    
    def __str__(self):
        return f"{self.roll_no}:{self.username}"

class LibraryBooks(models.Model):

    stars = (
            ('1' ,'*'),
            ('2','**'),
            ('3','***'),
            ('4','****'),
            ('5','*****'),
            )
    title = models.CharField(max_length = 100,)
    popularity = models.CharField(max_length = 5, choices = stars)
    #count = models.IntegerField()
    borrowed = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.title}=>Borrowed:{self.borrowed}'

class Book(models.Model):

    #title = models.CharField(max_length = 100, )
    
    date = models.DateField()
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    #user = models.ForeignKey(User, on_delete=models.SET_NULL, null = True)
    library_book = models.OneToOneField(LibraryBooks, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.library_book.title



class Genre(models.Model):
    name = models.CharField(max_length = 100)
    # on_delete = cascade because if the book of related genre is deleted there will be no existence of that genre
    books = models.ForeignKey(LibraryBooks, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
     
class Author(models.Model):
    name = models.CharField(max_length = 100)
    email = models.EmailField()
    # on_delete = cascade because if there is no book then there will be record of no author
    books = models.ManyToManyField(Book)

    def __str__(self):
        return self.name
