from django.shortcuts import get_object_or_404, redirect, render
from .forms import EmployeeForm
from .models import Employee

# Create your views here.
def employee_list(request):
    context = {'employees':Employee.objects.all()}
    return render(request, 'employee/list.html', context)

def create_employee(request):
    context = {}
    if request.method == 'GET':
        form = EmployeeForm()
        context = {'form':form}  
        return render(request, 'employee/create.html', context)

    if request.method == 'POST':
        form = EmployeeForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/employee')


def edit_employee(request, id):
    #emp = Employee.objects.get(pk=id)
    emp = get_object_or_404(Employee, pk=id)
    if request.method == 'GET':
        #data = {'emp_name':emp.emp_name,'emp_email':emp.emp_email,'emp_contact':emp.emp_contact}
        # form = Employee(data)
        # Below one is the djangonic way to do it
        form = EmployeeForm(None, instance=emp) 
        context = {'form':form}
        return render(request, 'employee/edit.html', context)

    if request.method == 'POST':
        form = EmployeeForm(request.POST, instance=emp)
        if form.is_valid():
            form.save()
            return redirect('/employee')

def employee_detail(request, id):
    emp = get_object_or_404(Employee, pk=id)
    return render(request, 'employee/detail.html', {'emp':emp})

def delete_employee(request, id):
    emp = get_object_or_404(Employee, pk=id)
    emp.delete()
    return redirect('/employee')    

def index(request):
    return render(request, 'employee/index.html')




    
