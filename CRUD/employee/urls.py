from employee.views import create_employee, delete_employee, edit_employee, employee_detail, employee_list
from django.urls import path,include

urlpatterns = [
        path('', employee_list, name='employee_list'),
        path('create/', create_employee, name='create_employee'),
        path('edit/<int:id>', edit_employee, name='edit_employee'),
        path('detail/<int:id>', employee_detail, name='employee_detail'),
        path('delete/<int:id>', delete_employee, name='delete_employee'),

        ]

