from .views import CreateStudentView, DeleteStudentView, UpdateStudentView, StudentListView
from django.urls import path,include

urlpatterns = [
        path('', StudentListView.as_view(), name='student_list'),
        path('create/', CreateStudentView.as_view(), name='create_student'),
        path('edit/<str:pk>', UpdateStudentView.as_view(), name='update_student'),
        #path('detail/<int:id>', employee_detail, name='employee_detail'),
        path('delete/<str:pk>', DeleteStudentView.as_view(), name='delete_student'),

        ]

