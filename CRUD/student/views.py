from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
#from django.views.generic.detail import DetailView
from .models import Student

# Create your views here.
class StudentListView(ListView):
    model = Student
    template_name = 'student/list.html' 
    queryset = Student.objects.all().order_by('roll_num')
    context_object_name = 'students' # by default it's name is object_list

class StudentDetailView(DetailView):
    model = Student
    template_name = 'student/detail.html'
    context_object_name = 'student'

class CreateStudentView(CreateView):
    model = Student
    template_name = 'student/create.html'
    fields = "__all__"
    success_url = '/student/'
    
class UpdateStudentView(UpdateView):
    model = Student
    fields = "__all__"
    template_name = 'student/edit.html'
    success_url = '/student/'

class DeleteStudentView(DeleteView):
    model = Student
    template_name = 'student/delete.html'
    success_url = '/student/'
    context_object_name = 'student'
     
