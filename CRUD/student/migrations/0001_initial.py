# Generated by Django 3.2.11 on 2022-02-08 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('roll_num', models.CharField(max_length=12, primary_key=True, serialize=False)),
                ('fullname', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=254)),
                ('contact', models.CharField(max_length=10)),
            ],
        ),
    ]
