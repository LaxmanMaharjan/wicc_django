from django.db import models

# Create your models here.
class Student(models.Model):
    roll_num = models.CharField(max_length = 12, primary_key = True)
    fullname = models.CharField(max_length = 30)
    email = models.EmailField()
    contact = models.CharField(max_length = 10)

    def __str__(self):
        return f"{self.roll_num}:{self.fullname}"
