from django.db import models

# Create your models here.
class User(models.Model):
   username = models.CharField(max_length = 100, primary_key = True) 
   first_name = models.CharField(max_length = 50)
   last_name = models.CharField(max_length = 50)
   email = models.EmailField()
   password = models.CharField(max_length = 50)

   def __str__(self):
       return self.username

class Blog(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length = 100)
    content = models.CharField(max_length = 1000)
    created_date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='blog-images/', null=True, blank=True)

    def __str__(self):
        return f"{self.author}:{self.title}"

class BlogComment(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    content = models.CharField(max_length = 1000)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.blog}:{self.content}"
    

