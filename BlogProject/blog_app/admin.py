from django.contrib import admin
from .models import User, Blog, BlogComment

# Register your models here.
class UserAdmin(admin.ModelAdmin):
    list_display = ['username','email']

class BlogAdmin(admin.ModelAdmin):
    list_display = ['author','title','created_date']

class BlogCommentAdmin(admin.ModelAdmin):
    list_display = ['blog','content','created_date']

admin.site.register(User,UserAdmin)
admin.site.register(Blog,BlogAdmin)
admin.site.register(BlogComment,BlogCommentAdmin)
