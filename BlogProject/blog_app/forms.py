from django import forms
from .models import Blog, BlogComment

class BlogForm(forms.ModelForm):

    class Meta:
        model = Blog
        fields = ['title', 'content', 'image']


class BlogCommentForm(forms.ModelForm):

    class Meta:
        model = BlogComment
        fields = ['content']
