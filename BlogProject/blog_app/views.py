from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.generic import ListView, TemplateView
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from .forms import BlogCommentForm
from .models import Blog, BlogComment, User
from django.core import serializers
from django.views import View

from rest_framework import viewsets
from .serializers import BlogSerializer,UserSerializer,BlogCommentSerializer
# Create your views here.

class HomeView(TemplateView):
    template_name = 'blog/index.html'


class BlogListView(ListView):
    model = Blog
    template_name = 'blog/list.html'
    queryset = Blog.objects.all().order_by('-created_date')
    context_object_name = 'blogs'

class BlogDetailView(DetailView):
    model = Blog
    template_name = 'blog/detail.html'
    context_object_name = 'blog'

    def get_context_data(self, *args, **kwargs):
        context =  super().get_context_data(*args, **kwargs)
        context['comments'] = BlogComment.objects.filter(blog=self.object).order_by('-created_date')
        context['form'] = BlogCommentForm()
        return context

class BlogCommentView(View):
    def post(self, request, *args, **kwargs):

        blog_id = request.POST.get('blog_id')
        blog = Blog.objects.get(id=blog_id)
        data = request.POST.dict()
        data.pop('blog_id')
        data['blog'] = blog

        form = BlogCommentForm(data)
        if form.is_valid():
            # Model.save() does either INSERT or UPDATE of an object in a DB, while Model.objects.create() does only INSERT.
            comment= BlogComment(blog=blog,content=data['content'])
            comment.save()
            ser_instance = serializers.serialize('json', [comment,]) # list of dictionary with necessary info
            return JsonResponse({'instance': ser_instance,})

class BlogCommentUpdateView(UpdateView):
    model = BlogComment
    fields = ['content']        
    template_name = 'blog/edit.html'

    def get_context_data(self, **kwargs):
        context =  super().get_context_data( **kwargs)
        context['id'] = self.object.pk
        return context

    def post(self, request, *args, **kwargs):
        comment_id = request.POST.get('comment_id')
        comment = BlogComment.objects.get(id=comment_id)
        # getting only required field for BlogCommentForm
        blog_id = request.POST.get('blog_id')
        blog = Blog.objects.get(id=blog_id)
        data = request.POST.dict()
        data.pop('blog_id')
        data.pop('comment_id')
        data['blog'] = blog
        print('from post')
        print(request.POST)

        print(comment)
        form = BlogCommentForm(data, instance=comment)
        if form.is_valid():
            # Model.save() does either INSERT or UPDATE of an object in a DB, while Model.objects.create() does only INSERT.
            comment = form.save()
            ser_instance = serializers.serialize('json', [comment,]) # list of dictionary with necessary info
            return JsonResponse({'instance': ser_instance,})       
            #return JsonResponse({'instance': 'hello'})       

    
class BlogCommentDeleteView(DeleteView):
    
    def post(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        comment = get_object_or_404(BlogComment,pk=pk)
        comment.delete()
        return JsonResponse({'instance': 'hello'})       
                

#class BlogCommentCreateView(CreateView):
#    model = BlogComment
#    template_name = 'blog/create.html'
#    fields = ['blog','content']
#    success_url = '/'
#
#    def form_valid(self, form):
#        print('form_valid')
#        instance = form.save()
#        # how to get blog_obj
#        #comments = list(BlogComment.objects.filter(blog=blog_obj).order_by('-created_date'))
#        ser_instance = serializers.serialize('json', [instance,]) # list of dictionary with necessary info
#        #ser_instance = serializers.serialize('json', comments) # list of dictionary with necessary info
#        #print(instance)
#        #print(ser_instance)
#        return JsonResponse({'instance':ser_instance})
#        
#    def form_invalid(self, form):
#        print('form invalid')
#        return super().form_invalid(form)

#class GetBlogAjaxView(View):
#    def get(self, request, *args, **kwargs):
#        blog_id = self.request.GET.get('blog_id')
#        print('blog_id',blog_id)
#        blog = Blog.objects.filter(id=blog_id)
#        print('blog',blog[0])
#        print(type(blog[0]))
#
#        return JsonResponse({'blog':blog[0].title}, safe=False)



## for rest api
class BlogViewset(viewsets.ModelViewSet):
    serializer_class = BlogSerializer
    queryset = Blog.objects.all()

class UserViewset(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

class BlogCommentViewset(viewsets.ModelViewSet):
    serializer_class = BlogCommentSerializer
    queryset = BlogComment.objects.all()
