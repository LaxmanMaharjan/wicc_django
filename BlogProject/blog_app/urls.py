from django.urls.conf import include
from blog_app.views import BlogCommentDeleteView, BlogCommentUpdateView, BlogCommentView, BlogCommentViewset, BlogDetailView, BlogListView, BlogViewset, UserViewset
from django.urls import path
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'blog_api',BlogViewset, basename='blog_api')
router.register(r'blog_comment_api',BlogCommentViewset, basename='blog_comment_api')
router.register(r'user_api',UserViewset, basename='user_api')

urlpatterns = [
        path('', BlogListView.as_view(), name='blog_list'),
        path('',include(router.urls)),
        path('detail/<str:pk>', BlogDetailView.as_view(), name='blog_detail'),
        path('create', BlogCommentView.as_view(), name='create_blogcomment'),
        path('update/<int:pk>',BlogCommentUpdateView.as_view() , name='update_blogcomment'),
        path('delete/<int:pk>',BlogCommentDeleteView.as_view() , name='delete_blogcomment'),
        
        
        ]
