from .models import Blog,BlogComment,User
from rest_framework import serializers

class BlogSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Blog
        fields = ['author', 'title','content','created_date']

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'

class BlogCommentSerializer(serializers.ModelSerializer):
    blog = serializers.PrimaryKeyRelatedField(queryset = Blog.objects.all())
    
    class Meta:
        model = BlogComment
        fields = ['blog','content']
        depth = 1

