from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.edit import CreateView
from .forms import CommentForm
from .models import Comment
from django.core import serializers
# Create your views here.

def index(request):
    return render(request, 'base.html')

class CommentListView(ListView):
    model = Comment
    template_name = 'comment/list.html'
    queryset = Comment.objects.all().order_by('-time')
    context_object_name = 'comments'

    def get_context_data(self, *args, **kwargs):
        context =  super().get_context_data(*args, **kwargs)
        context['form'] = CommentForm()
        return context

class CommentCreateView(CreateView):
    model = Comment
    template_name = 'comment/create.html'
    fields = ['content']
    success_url = '/comment'

    def form_valid(self, form):
        instance = form.save()
        comments = list(Comment.objects.all().order_by('-time'))
        #ser_instance = serializers.serialize('json', [instance,]) # list of dictionary with necessary info
        ser_instance = serializers.serialize('json', comments) # list of dictionary with necessary info
        print(instance)
        print(ser_instance)
        return JsonResponse({'instance':ser_instance})
        
