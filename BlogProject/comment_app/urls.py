from comment_app.views import CommentCreateView, CommentListView
from django.urls import path

urlpatterns = [
        path('', CommentListView.as_view(), name='comment_list'),
        path('create/', CommentCreateView.as_view(), name='create_comment'),
        ]
